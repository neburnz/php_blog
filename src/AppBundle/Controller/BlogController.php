<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogController extends Controller
{
  /**
   * @Route("/blog")
   */
  public function indexAction()
  {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
    
    $posts = $repository->findAll();
    
    return $this->render('blog/index.html.twig', array('entries' => $posts));
  }
}
