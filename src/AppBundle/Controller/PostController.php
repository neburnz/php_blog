<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
  /**
   * @Route("/post/create")
   */
  public function createAction(Request $request)
  {
    $post = new Post();

    $form = $this->createFormBuilder($post)
	    ->add('title', TextType::class)
	    ->add('content', TextareaType::class)
	    ->add('save', SubmitType::class, array('label' => 'Save'))
	    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
	$post->setDraft(false);
	$post->setSlug($this->slugify($post->getTitle()));
	$em = $this->getDoctrine()->getManager();
	$em->persist($post);
	$em->flush();

	$this->addFlash('message','El post se ha guardado correctamente');

	return $this->redirectToRoute('app_post_notify');
    }

    return $this->render('post/create.html.twig', array('form' => $form->createView(),));
  }

  /**
	  * @Route("/post/{slug}")
   */
  public function showAction($slug)
  {
	  $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
	  $post = $repository->findOneBy(array('slug' => $slug));
	  return $this->render('post/post.html.twig', array('post' => $post));
  }

  /**
	  * @Route("/message")
   */
  public function notifyAction()
  {
	  return $this->render('post/notify.html.twig');
  }

  private function slugify($text)
  {
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');
	  
	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	  
	  // lowercase
	  $text = strtolower($text);
	  
	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);
	  
	  if (empty($text))
	  {
	  	return 'n-a';
	  }
	  
	  return $text;
  }

}
