<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
	* @ORM\Entity
	* @ORM\Table(name="post")
 */
class Post
{
	/**
		* @ORM\Column(type="integer")
		* @ORM\Id
		* @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
		* @ORM\Column(type="string", length=250)
	 */
	protected $title;

	/**
		* @ORM\Column(type="string", length=250)
	 */
	protected $slug;
	
	/**
		* @ORM\Column(type="text")
	 */
	protected $content;
	
	/**
		* @ORM\Column(type="boolean")
	 */
	protected $draft;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Post
     */
    public function setSlug($slug)
    {
	    $this->slug = $slug;

	    return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
	    return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set draft
     *
     * @param boolean $draft
     *
     * @return Post
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * Get draft
     *
     * @return boolean
     */
    public function isDraft()
    {
        return $this->draft;
    }

    /**
     * Get draft
     *
     * @return boolean
     */
    public function getDraft()
    {
        return $this->draft;
    }
}
